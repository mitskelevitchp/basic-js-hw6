// Теоретичні питання
/*
1. Екранування використовується з метою вирішення конфліктів синтаксису - воно дозволяє використовувати службові символи мови в "діалозі" з користувачем. Наприклад, виводити в текстовому контенті "[]", "\", "*", "+" та інші. Оскільки ці символи є частиною лексичного коду мови програмування, без екранування їх використання в інструментах інтерактиву веб-сторінки було б або важким, або неможливим - спричиняло б помилки.
2. Функції створюються двома способами: оголошенням функції (function declaration) або застосуванням вираження (function expression). Друга створюється оголошенням змінної з присвоєнням їй значення, що повертається в тілі функції (після оголошення перемінної та оператора присвоєння йде код функції). Перша - без використання змінної (код функції є частиною js-коду).
3. "Підняття" - це встановлена в JS пріоритетність читання коду. Оголошення перемінних та функцій має вищий пріоритет зчитування за їх ініціалізацію (присвоєння значення). Для перемінних це працює з оператором var, для функцій - з function declaration.
Так, наприклад, якщо присвоїти значення змінній var до її оголошення (a = 5; var a;), код працюватиме. Якщо присвоїти значення змінній let чи const до їх оголошення, код не працюватиме. Якщо викликати function declaration до самого коду функції, код працюватиме. Якщо викликати function expression (змінну, яка посилається на код функції) до самого коду функції, код не працюватиме.
*/

// Практичне завдання
/* Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.
Технічні вимоги:
1. При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
2. Створити метод getAge() який повертатиме скільки користувачеві років.
3. Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
4. Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта. */

const newUser = {};
function createNewUser() {
  let askFirstName = prompt("Вкажіть ваше ім'я:");
  let askLastName = prompt("Вкажіть ваше прізвище:");
  let askBirthday = prompt(
    "Вкажіть дату вашого народження у форматі дд.мм.рррр:"
  );

  newUser.firstName = askFirstName;
  newUser.lastName = askLastName;
  newUser.birthday = askBirthday;

  // change format dd.mm.yyyy -> yyyy.mm.dd
  let incorrectDate = newUser.birthday;
  let dayOfBirthday = incorrectDate.slice(0, 2);
  let monthOfBirthday = incorrectDate.slice(3, 5);
  let yearOfBirthday = incorrectDate.slice(6);
  let correctDate =
    yearOfBirthday + "-" + monthOfBirthday + "-" + dayOfBirthday;

  // newUser.getLogin = function () {
  //   return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
  // };

  newUser.getAge = function () {
    let today = new Date();
    let birthDate = new Date(correctDate);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  };

  newUser.getPassword = function () {
    return (
      this.firstName[0].toUpperCase() +
      this.lastName.toLowerCase() +
      yearOfBirthday
    );
  };
}

createNewUser();
console.log(newUser);
console.log(newUser.getPassword());
console.log(newUser.getAge());
